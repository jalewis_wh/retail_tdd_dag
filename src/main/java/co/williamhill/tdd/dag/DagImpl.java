package co.williamhill.tdd.dag;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class DagImpl implements Dag<Node> {
    @Override
    public Collection<Node> getDescendantsForNode(Node givenNode) {
        return getDescendents(givenNode);
    }

    private Set<Node> getDescendents(Node givenNode) {
        Set<Node> descendents = new HashSet<>();

        if (givenNode.getChildren().isEmpty()) {
            return descendents;
        }

        for (Node child : givenNode.getChildren()) {
            descendents.add(child);
            descendents.addAll(getDescendents(child));
        }

        return descendents;
    }

    @Override
    public Collection<Node> getAncestorsForNode(Node givenNode) {
        return getAncestors(givenNode);
    }

    private Set<Node> getAncestors(Node givenNode) {
        Set<Node> ancestors = new HashSet<>();

        if (givenNode.getParents().isEmpty()) {
            return ancestors;
        }

        for (Node parent : givenNode.getParents()) {
            ancestors.add(parent);
            ancestors.addAll(getAncestors(parent));
        }

        return ancestors;
    }
}
