package co.williamhill.tdd.dag;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class NodeImpl implements Node {

    static final String CAN_NOT_ADD_CHILD_MESSAGE = "You can not add this as a child, this node is already a parent";
    static final String CAN_NOT_ADD_PARENT_MESSAGE = "You can not add this as a parent, this node is already a child";
    static final String CAN_NOT_ADD_ITSELF_AS_PARENT = "Node itself can not be a parent or child";

    private final String name;
    private Set<Node> children;
    private Set<Node> parents;

    public NodeImpl(String name) {
        this.name = name;
        children = new HashSet<>();
        parents = new HashSet<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void addChild(Node child) {
        if (getParents().contains(child)) {
            throw new IllegalArgumentException(CAN_NOT_ADD_CHILD_MESSAGE);
        }

        if (this.equals(child)) {
            throw new IllegalArgumentException(CAN_NOT_ADD_ITSELF_AS_PARENT);
        }
        children.add(child);
    }

    @Override
    public void addParent(Node parent) {
        if (getChildren().contains(parent)) {
            throw new IllegalArgumentException(CAN_NOT_ADD_PARENT_MESSAGE);
        }

        if (this.equals(parent)) {
            throw new IllegalArgumentException(CAN_NOT_ADD_ITSELF_AS_PARENT);
        }
        parents.add(parent);
    }

    @Override
    public Collection<Node> getChildren() {
        return children;
    }

    @Override
    public Collection<Node> getParents() {
        return parents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NodeImpl)) return false;
        NodeImpl node = (NodeImpl) o;
        return Objects.equals(name, node.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
